require "rails_helper"

feature "user creates person" do
  scenario "with valid data" do
    # Visit a page
	visit new_person_url

	# Fill in a text field
	fill_in "First name", with: "Josh"

	# Click on a button
	click_button "Create Person"

	# Check for text on the page
	expect(page).to have_content("Person created.")
    expect(page).to have_content("Josh")
  end
end
